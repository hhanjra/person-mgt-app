import { Component, OnInit } from '@angular/core';
import {HttpPersonServiceService, Person} from '../service/http-person-service.service'

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  persons:Person[];

  constructor(private httpPersonService : HttpPersonServiceService) { }

  ngOnInit() {
    this.httpPersonService.getPersons().subscribe(
     response =>this.handleSuccessfulResponse(response),
    );
  }
  handleSuccessfulResponse(response)
  {
    this.persons = response;
  }

}
