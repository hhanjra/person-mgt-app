import { Component, OnInit } from '@angular/core';
import {HttpPersonServiceService, Person} from '../service/http-person-service.service'

@Component({
  selector: 'app-search-person-by-name',
  templateUrl: './search-person-by-name.component.html',
  styleUrls: ['./search-person-by-name.component.css']
})
export class SearchPersonByNameComponent implements OnInit {
  person:Person = new Person("","");
  constructor(private httpPersonService : HttpPersonServiceService) { }

  ngOnInit() {
    
  }
  handleSuccessfulResponse(response)
  {
    this.person = response;
  }

  createEmployee(name:string): void {
    this.httpPersonService.getPersonByName(name)
        .subscribe( data => this.handleSuccessfulResponse(data),
        );
  };

}
