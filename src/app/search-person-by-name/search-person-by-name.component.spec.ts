import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPersonByNameComponent } from './search-person-by-name.component';

describe('SearchPersonByNameComponent', () => {
  let component: SearchPersonByNameComponent;
  let fixture: ComponentFixture<SearchPersonByNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPersonByNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPersonByNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
