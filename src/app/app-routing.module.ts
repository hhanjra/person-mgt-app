import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonComponent} from './person/person.component';
import { SearchPersonByNameComponent } from './search-person-by-name/search-person-by-name.component'


const routes: Routes = [
  { path:'', component: PersonComponent},
  { path:'search',component : SearchPersonByNameComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
