import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

export class Person {
  constructor(
    public firstName:string,
    public lastName:string,
  ){}
}

@Injectable({
  providedIn: 'root'
})
export class HttpPersonServiceService {

  constructor(private httpClient:HttpClient) { }

  getPersons() {
    
   return this.httpClient.get<Person[]>('http://localhost:8080/person/list');
  }

  getPersonByName(name:string) {
    return this.httpClient.get<Person>('http://localhost:8080/person/search/'+name);
  }
}
