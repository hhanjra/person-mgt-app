import { TestBed } from '@angular/core/testing';

import { HttpPersonServiceService } from './http-person-service.service';

describe('HttpPersonServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpPersonServiceService = TestBed.get(HttpPersonServiceService);
    expect(service).toBeTruthy();
  });
});
