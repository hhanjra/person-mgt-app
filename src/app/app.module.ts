import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PersonComponent } from './person/person.component';
import {HttpPersonServiceService } from './service/http-person-service.service'
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
import { SearchPersonByNameComponent } from './search-person-by-name/search-person-by-name.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PersonComponent,
    SearchPersonByNameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [HttpPersonServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
